package com.orange.fastronomy.maker.burger.domain;

import org.eclipse.microprofile.openapi.annotations.media.Schema;

import jakarta.persistence.*;
import java.util.List;

@Schema(description = "A burger with its ingredients")
@Embeddable
public class Burger {
    @Schema(description = "Burger's id")
    @Column
    private String id;

    @Schema(description = "The constant 'burger'")
    @Column
    private String name;

    @Schema(description = "The burger is composed of these ingredients")
    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "burger_id")    private List<Ingredient> ingredients;

    public Burger() {
    }

    public Burger(String id, String name, List<Ingredient> ingredients) {
        this.id = id;
        this.name = name;
        this.ingredients = ingredients;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Ingredient> getIngredients() {
        return ingredients;
    }

    public void setIngredients(List<Ingredient> ingredients) {
        this.ingredients = ingredients;
    }
}
