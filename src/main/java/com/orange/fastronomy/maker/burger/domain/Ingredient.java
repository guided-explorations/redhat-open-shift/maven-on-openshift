package com.orange.fastronomy.maker.burger.domain;

import org.eclipse.microprofile.openapi.annotations.media.Schema;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;

@Schema(description = "An ingredient")
@Entity(name = "ingredients")
public class Ingredient {
    @Schema(description = "Ingredient's id")
    @Id
    private String id;

    @Schema(description = "Ingredient's name. E.g.: bun, cheese...")
    @Column
    private String name;

    public Ingredient() {
    }

    public Ingredient(String id, String name) {
        this.id = id;
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
