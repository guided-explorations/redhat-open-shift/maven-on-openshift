package com.orange.fastronomy.maker.burger.web;

import org.eclipse.microprofile.config.inject.ConfigProperty;

import jakarta.enterprise.inject.Instance;
import jakarta.ws.rs.container.ContainerRequestContext;
import jakarta.ws.rs.container.ContainerResponseContext;
import jakarta.ws.rs.container.ContainerResponseFilter;
import jakarta.ws.rs.ext.Provider;

/**
 * Adds the hostname as a http response header
 */
@Provider
public class HostnameHeaderSender implements ContainerResponseFilter {
    @ConfigProperty(name = "application.hostname")
    protected Instance<String> hostname;

    @Override
    public void filter(ContainerRequestContext requestContext, ContainerResponseContext responseContext) {
        responseContext.getHeaders().add("X-PROCESSED-BY", hostname.get());
    }
}
