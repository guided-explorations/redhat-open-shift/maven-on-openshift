#!/bin/bash

set -e

echo "[burger-maker] pre apply script: create a MariaDB database if does not exist"

if ! oc get deploymentconfig "$environment_name-db" > /dev/null 2>&1 ; then
  echo "Database does not exist, create new"
  oc new-app mariadb-persistent --param="DATABASE_SERVICE_NAME=$environment_name-db" --param="MYSQL_DATABASE=burgermakerdb" --param="VOLUME_CAPACITY=64Mi" --labels="app=$environment_name"
  oc get deploymentconfig "$environment_name-db" 2> /dev/null || oc set resources dc/"$environment_name-db" --limits=cpu=250m,memory=512Mi --requests=cpu=100m,memory=256Mi
fi