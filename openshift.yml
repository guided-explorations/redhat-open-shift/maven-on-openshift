# The generic OpenShift template used to instantiate burger-maker objects
apiVersion: template.openshift.io/v1
kind: Template
metadata:
  name: burger-maker-template
  description: a burger-maker deployment template
# template parameters
parameters:
  - name: environment_name
    description: "the application target name to use in this environment (provided by GitLab CI template)"
    required: true
  - name: hostname
    description: "the environment hostname (provided by GitLab CI template)"
    required: true
objects:
# === ImageStream
- apiVersion: image.openshift.io/v1
  kind: ImageStream
  metadata:
    labels:
      app: ${environment_name}
    name: ${environment_name}
# === BuildConfig (binary source based on registry.access.redhat.com/ubi8/openjdk-21 image)
- apiVersion: build.openshift.io/v1
  kind: BuildConfig
  metadata:
    labels:
      app: ${environment_name}
    name: ${environment_name}
  spec:
    output:
      to:
        kind: ImageStreamTag
        name: ${environment_name}:latest
    postCommit: {}
    resources: {}
    runPolicy: Serial
    source:
      binary: {}
      type: Binary
    strategy:
      sourceStrategy:
        from:
          kind: ImageStreamTag
          name: openjdk-21:1.19-4.1715070809
      type: Source
# === Service
- apiVersion: v1
  kind: Service
  metadata:
    annotations:
      description: Exposes and load balances the application pods.
    labels:
      app: ${environment_name}
    name: ${environment_name}
  spec:
    ports:
    - name: http
      port: 8080
      protocol: TCP
      targetPort: 8080
    selector:
      app: ${environment_name}
    type: ClusterIP
    sessionAffinity: None
# === DeploymentConfig
- apiVersion: apps.openshift.io/v1
  kind: DeploymentConfig
  metadata:
    annotations:
      description: The deployment configuration of application.
    labels:
      app: ${environment_name}
    name: ${environment_name}
  spec:
    replicas: 1
    revisionHistoryLimit: 2
    selector:
      app: ${environment_name}
    strategy:
      type: Rolling
      rollingParams:
        timeoutSeconds: 3600
    template:
      metadata:
        labels:
          app: ${environment_name}
      spec:
        containers:
        - env:
          - name: MYSQL_SERVICE_NAME
            value: ${environment_name}-db
          - name: MYSQL_DATABASE
            valueFrom:
              secretKeyRef:
                key: database-name
                name: ${environment_name}-db
          - name: MYSQL_USER
            valueFrom:
              secretKeyRef:
                key: database-user
                name: ${environment_name}-db
          - name: MYSQL_PASSWORD
            valueFrom:
              secretKeyRef:
                key: database-password
                name: ${environment_name}-db
          - name: KUBERNETES_NAMESPACE
            valueFrom:
              fieldRef:
                fieldPath: metadata.namespace
          image: ${environment_name}:latest
          imagePullPolicy: IfNotPresent
          livenessProbe:
            httpGet:
              path: /health/live
              port: 8080
              scheme: HTTP
            initialDelaySeconds: 180
          name: ${environment_name}
          ports:
          - containerPort: 8080
            name: http
            protocol: TCP
          - containerPort: 9779
            name: prometheus
            protocol: TCP
          - containerPort: 8778
            name: jolokia
            protocol: TCP
          readinessProbe:
            httpGet:
              path: /health/ready
              port: 8080
              scheme: HTTP
            initialDelaySeconds: 10
          securityContext:
            privileged: false
    triggers:
    - type: ConfigChange
    - imageChangeParams:
        automatic: true
        containerNames:
        - spring-boot
        from:
          kind: ImageStreamTag
          name: ${environment_name}:latest
      type: ImageChange
# === Route
- apiVersion: route.openshift.io/v1
  kind: Route
  metadata:
    annotations:
      description: The route exposes the service at a hostname.
    labels:
      app: ${environment_name}
    name: ${environment_name}
  spec:
    host: ${hostname}
    tls:
      termination: edge
      insecureEdgeTerminationPolicy: Redirect
    port:
      targetPort: 8080
    to:
      kind: Service
      name: ${environment_name}
