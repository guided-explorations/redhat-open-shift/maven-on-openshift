#!/bin/bash

set -e

echo "[burger-maker] post apply script: trigger a build"

# prepare build resources
mkdir -p target/openshift/deployments && cp target/quarkus-app/quarkus-run.jar /deployments

# trigger build (jar -> Docker): this will trigger a deployment
oc start-build "$environment_name" --from-dir=target/openshift --wait --follow
